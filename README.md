# SNIPassThru

_SNIPassThru_ is a TLS proxy which, based on the [TLS SNI][1] contained in TLS
handshakes. It passes through the request intact to the host found in SNI.
The proxy does not need the TLS encryption keys and can not decrypt the TLS traffic.

[1]: https://en.wikipedia.org/wiki/Server_Name_Indication

_SNIPassThru_ is meant to be simple to use and configure, with sane defaults and
few parameters.

_SNIPassThru_ is a simplified version of [sniproxy][1] and its [fork][2] bred for
really specific use case of pass-through proxy.

[1]: https://github.com/atenart/sniproxy
[2]: https://github.com/brave/sniproxy

## How to use

Use `go build` to build _SNIPassThru_.

```shell
$ go build ./cmd/snipassthru
```

_SNIPassThru_ can be bound to a custom address or port using the `-bind` command
line option.

```shell
$ snipassthru -bind 192.168.0.1:8443
```

_SNIPassThru_ starts an HTTP server to handle HTTP>HTTPS redirects (see
below). By default, this server binds to `:80` but a different address and/or
port can be selected using the `-http-bind` option.

```shell
$ snipassthru -http-bind 192.168.0.1:8080
```

The log level can be controlled using the `-log-level` option. It takes one of
the following values: `error`, `warn`, `info` or `debug`. By default it is set
to `info`.

```shell
$ snipassthru -log-level error
```

_SNIPassThru_ also has the ability to block or allow connections based on the
client IP address. Single IPs or subnets (using a CIDR range) are supported.
Use `-allow` and `-deny` command line flags to specify them. The most specific
range wins. If the ranges are the same, deny wins.

```shell
$ snipassthru -deny 192.168.0.0/22 -allow 192.168.1.8/29 -allow 192.168.0.2
```

If at least one `-allow` flag specified then all IPs are denied automatically.
```shell
# Implies that all IPs other than 192.168.0.42 are not allowed
$ snipassthru -allow 192.168.0.42
# Equivalent to
$ snipassthru -deny 0.0.0.0/0 -deny ::/0 -allow 192.168.0.42
```

_SNIPassThru_ redirects HTTP requests to HTTPS. It redirects both genuine
HTTP requests coming to an HTTP server started by _SNIPassThru_ and
unexpected HTTP requests coming on the TLS server.
