// Copyright (C) 2019-2022 Antoine Tenart <antoine.tenart@ack.tf>
// Copyright (C) 2022 Sergey Zagursky <gvozdoder@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"flag"
	"fmt"
	"net"
	"net/netip"
	"strconv"
	"strings"

	"gitlab.com/zagursky/snipassthru/internal/log"
	"gitlab.com/zagursky/snipassthru/internal/snipassthru"
)

var (
	allowList           []*net.IPNet
	denyList            []*net.IPNet
	bind                string
	http                string
	dns                 string
	logLevel            string
	dnsTargetIP         = netip.MustParseAddr("127.0.0.1")
	dnsTTLSeconds       int
	preferGoDNSResolver bool
)

func init() {
	flag.StringVar(&bind, "bind", ":443", "Address and port to bind to.")
	flag.StringVar(&http, "http-bind", ":80", "Address and port to bind to, listening for HTTP traffic to redirect to its HTTPS counterpart.")
	flag.StringVar(&dns, "dns-bind", ":53", "Address and port to bind DNS server to.")
	flag.StringVar(&logLevel, "log-level", "info", "Log level (debug, info, warn, err)")
	flag.IntVar(&dnsTTLSeconds, "dns-ttl-seconds", 600, "DNS TTL")
	flag.BoolVar(&preferGoDNSResolver, "use-go-dns-resolver", false, "Prefer Go DNS resolver")

	flag.Var((*ipRangeListFlag)(&allowList), "allow", "IP range or address to whitelist. All addresses are blocked by default if used.")
	flag.Var((*ipRangeListFlag)(&denyList), "deny", "IP range or address to blacklist.")
	flag.Var((*ipAddressFlag)(&dnsTargetIP), "dns-target-ip", "IP address to put into DNS answers")
}

type ipRangeListFlag []*net.IPNet
type ipAddressFlag netip.Addr

func (f *ipRangeListFlag) String() string {
	if f == nil {
		return ""
	}

	var rss []string
	for _, r := range *f {
		rss = append(rss, r.String())
	}

	return strings.Join(rss, ",")
}

func (f *ipRangeListFlag) Set(s string) error {
	subnet, err := snipassthru.ParseRange(s)
	if err != nil {
		return fmt.Errorf("failed to parse IP range '%s': %w", s, err)
	}

	*f = append(*f, subnet)
	return nil
}

func (f *ipRangeListFlag) Get() any {
	return ([]*net.IPNet)(*f)
}

func (f *ipAddressFlag) String() string {
	if f == nil {
		return ""
	}

	return ((*netip.Addr)(f)).String()
}

func (f *ipAddressFlag) Set(s string) error {
	addr, err := netip.ParseAddr(s)
	if err != nil {
		return fmt.Errorf("failed to parse IP address '%s': %w", s, err)
	}

	*(*netip.Addr)(f) = addr
	return nil
}

func (f *ipAddressFlag) Get() any {
	return *(*netip.Addr)(f)
}

func main() {
	flag.Parse()

	if err := setLogLevel(); err != nil {
		log.Fatal(err)
	}

	if len(allowList) > 0 {
		// When using the allow flag, we should block all other IPs. Set Deny to match all IPs.
		_, all4, _ := net.ParseCIDR("0.0.0.0/0")
		_, all6, _ := net.ParseCIDR("::/0")
		denyList = append(denyList, all4)
		denyList = append(denyList, all6)
	}

	_, redirectPortStr, err := net.SplitHostPort(bind)
	if err != nil {
		log.Fatal(fmt.Sprintf("failed to split host and port: '%s'", bind), err)
	}

	redirectPort, err := strconv.Atoi(redirectPortStr)
	if err != nil {
		log.Fatal(fmt.Sprintf("failed to convert port to integer: '%s'", bind), err)
	}

	if preferGoDNSResolver {
		net.DefaultResolver.PreferGo = true
	}

	p := &snipassthru.Proxy{
		Config: snipassthru.Config{
			RedirectPort: redirectPort,
			Deny:         denyList,
			Allow:        allowList,
		},
	}

	go func() {
		log.Printf(log.INFO, "Listening HTTP on %s...", http)
		if err := p.HandleRedirect(http); err != nil {
			log.Fatal(err)
		}
	}()

	go func() {
		log.Printf(log.INFO, "Listening DNS on %s UDP (target=%s, TTL=%d)...", dns, dnsTargetIP, dnsTTLSeconds)
		if err := snipassthru.ServeDNS(dns, "udp", dnsTargetIP, dnsTTLSeconds); err != nil {
			log.Fatal(err)
		}
	}()

	go func() {
		log.Printf(log.INFO, "Listening DNS on %s TCP (target=%s, TTL=%d)...", dns, dnsTargetIP, dnsTTLSeconds)
		if err := snipassthru.ServeDNS(dns, "tcp", dnsTargetIP, dnsTTLSeconds); err != nil {
			log.Fatal(err)
		}
	}()

	log.Printf(log.INFO, "Listening HTTPS on %s...", bind)
	if err := p.ListenAndServe(bind); err != nil {
		log.Fatal(err)
	}
}

func setLogLevel() error {
	switch logLevel {
	case "debug":
		log.LogLevel = log.DEBUG
	case "info":
		log.LogLevel = log.INFO
	case "warn":
		log.LogLevel = log.WARN
	case "error":
		log.LogLevel = log.ERR
	default:
		return fmt.Errorf("invalid log level '%s'", logLevel)
	}
	return nil
}
