// Copyright (C) 2019-2022 Antoine Tenart <antoine.tenart@ack.tf>
// Copyright (C) 2022 Sergey Zagursky <gvozdoder@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package snipassthru

import (
	"fmt"
	"net"
)

// Config holds the entire current configuration.
type Config struct {
	// Port used when redirecting HTTP requests to their HTTPS counterpart.
	// Aka. the publicly accessible port of the TLS SNI server.
	RedirectPort int

	// Deny and Allow contain lists of IP ranges and/or addresses to
	// whitelist or blacklist for a given route. If Allow is used, all
	// addresses are then blocked by default.
	// The more specific subnet takes precedence, and Deny wins over Allow
	// in case none is more specific.
	Deny  []*net.IPNet
	Allow []*net.IPNet
}

// Parse a subnet string.
func ParseRange(subnet string) (*net.IPNet, error) {
	_, ipnet, err := net.ParseCIDR(subnet)
	if err == nil {
		return ipnet, nil
	}

	ip := net.ParseIP(subnet)
	if ip == nil {
		return nil, fmt.Errorf("Could not parse subnet " + subnet)
	}

	// IP is an IPv4 address, its CIDR should be /32.
	if v4 := ip.To4(); v4 != nil {
		return &net.IPNet{IP: ip, Mask: net.CIDRMask(32, 32)}, nil
	}

	// IP is an IPv6 address, its CIDR should be /128.
	return &net.IPNet{IP: ip, Mask: net.CIDRMask(128, 128)}, nil
}
