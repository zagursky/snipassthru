package snipassthru

import (
	"net/netip"

	"github.com/miekg/dns"
	"gitlab.com/zagursky/snipassthru/internal/log"
)

func ServeDNS(serveAddress string, net string, targetIP netip.Addr, ttlSeconds int) error {
	handleDNS := dns.HandlerFunc(func(rw dns.ResponseWriter, r *dns.Msg) {
		var m dns.Msg
		m.SetReply(r)
		for _, q := range r.Question {
			if q.Qtype == dns.TypeA && targetIP.Is4() {
				address := targetIP.As4()
				m.Answer = append(m.Answer, &dns.A{
					Hdr: dns.RR_Header{Name: q.Name, Rrtype: dns.TypeA, Class: dns.ClassINET, Ttl: uint32(ttlSeconds)},
					A:   address[:],
				})
				log.Printf(log.INFO, "[%s] Resolved A %s to %s", net, q.Name, netip.AddrFrom4(address))
			} else if q.Qtype == dns.TypeAAAA {
				address := targetIP.As16()
				m.Answer = append(m.Answer, &dns.AAAA{
					Hdr:  dns.RR_Header{Name: q.Name, Rrtype: dns.TypeAAAA, Class: dns.ClassINET, Ttl: uint32(ttlSeconds)},
					AAAA: address[:],
				})
				log.Printf(log.INFO, "[%s] Resolved AAAA %s to %s", net, q.Name, netip.AddrFrom16(address))
			}
		}

		if err := rw.WriteMsg(&m); err != nil {
			log.Printf(log.INFO, "Failed to write response to DNS request: %v", err)
		}

	})

	server := &dns.Server{
		Addr:    serveAddress,
		Net:     net,
		Handler: handleDNS,
	}

	return server.ListenAndServe()
}
